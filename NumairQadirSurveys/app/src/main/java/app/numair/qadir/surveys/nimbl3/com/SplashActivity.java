package app.numair.qadir.surveys.nimbl3.com;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

import app.numair.qadir.surveys.nimbl3.com.utilities.Common;

/**
 * Created by Numair on 8/13/2016.
 */

public class SplashActivity extends ActionBarActivity {

    private Intent intent;
    private final int SPLASH_TIME_OUT = 3000;
    private TextView txtAppName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        // text view label
        txtAppName = (TextView) findViewById(R.id.txtAppName);

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), Common.fontPath);

        // Applying font
        txtAppName.setTypeface(tf);

        new asyncSplash().execute();
    }

    class asyncSplash extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(SPLASH_TIME_OUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            switchActivity();
        }
    }

    private void switchActivity() {
        intent = new Intent(SplashActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }
}
