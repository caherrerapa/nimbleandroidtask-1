package app.numair.qadir.surveys.nimbl3.com.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.numair.qadir.surveys.nimbl3.com.R;
import app.numair.qadir.surveys.nimbl3.com.SurveyDetailsActivity;
import app.numair.qadir.surveys.nimbl3.com.bean.Survey;
import app.numair.qadir.surveys.nimbl3.com.utilities.AppToast;

public final class FragmentSurvey extends Fragment {
    private String mTitle = "";
    private String mDescription = "";
    private String imgUrl = "";
    private static final String KEY_CONTENT = "FragmentSurvey:Content";
    private static final String KEY_DESC = "FragmentSurvey:Desc";
    private static final String KEY_URL = "FragmentSurvey:Url";
    private static TextView txtDetail, txtHeading;
    private static ImageView imageResId;
    private static Button btnSurvey;

    public static Fragment newInstance(Survey survey) {
        FragmentSurvey fragment = new FragmentSurvey();

        fragment.mTitle = survey.getTitle();
        fragment.mDescription = survey.getDescription();
        fragment.imgUrl = survey.getCover_image_url();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT) && savedInstanceState.containsKey(KEY_DESC) && savedInstanceState.containsKey(KEY_URL)) {
            mTitle = savedInstanceState.getString(KEY_CONTENT);
            mDescription = savedInstanceState.getString(KEY_DESC);
            imgUrl = savedInstanceState.getString(KEY_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = new View(getActivity());
        view = (ViewGroup) inflater.inflate(R.layout.cell_dashboard_info, null, false);

        txtHeading = (TextView) view.findViewById(R.id.txtTitle);
        txtDetail = (TextView) view.findViewById(R.id.txtDesc);
        imageResId = (ImageView) view.findViewById(R.id.imgCover);
        btnSurvey = (Button) view.findViewById(R.id.btnSurvey);

        // Populate fields
        txtHeading.setText(mTitle);
        txtDetail.setText(mDescription);

        if (!imgUrl.isEmpty()) {
            Picasso.with(getActivity())
                    .load(imgUrl)
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(imageResId);

            btnSurvey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), SurveyDetailsActivity.class);
                    intent.putExtra("image_url", imgUrl);
                    startActivity(intent);
                }
            });
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mTitle);
        outState.putString(KEY_DESC, mDescription);
        outState.putString(KEY_URL, imgUrl);
    }

    @Override
    public void onResume() {
        System.gc();
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        System.gc();
        super.onDestroyView();
    }
}