package app.numair.qadir.surveys.nimbl3.com.utilities;

import android.content.Context;
import android.widget.Toast;

public class AppToast {
	public static void Show(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}
}
